const resultImages = document.querySelector(".resultImages");
const inputBox = document.querySelector(".inputBox");
const searchIcon = document.querySelector(".fa-magnifying-glass");
const hangOn = document.querySelector(".hangOn");
let pages = 1;
let keyword = "random";
let imageSearching = false;
async function searchImages() {
  const url = `https://api.unsplash.com/search/photos?page=${pages}&query=${keyword}&client_id=532DhvmivPLsXuyZXfA3dataZo9ohmpupVRwbjMvOGk&per_page=12`;
  const response = await fetch(url);
  const data = await response.json();
  const dataResults = data.results;
  if (data.total) {
    dataResults.map((eachData) => {
      const Image = document.createElement("img");
      Image.src = eachData.urls.small;
      const Link = document.createElement("a");
      Link.href = eachData.links.html;
      Link.innerHTML = eachData.alt_description;
      const Div = document.createElement("div");
      Div.classList.add("eachImage");
      Div.appendChild(Image);
      Div.appendChild(Link);
      resultImages.append(Div);
    });
    hangOn.style.display = "block";
  } else {
    const notFound = document.createElement("h4");
    notFound.innerHTML = "No images found";
    resultImages.append(notFound);
    hangOn.style.display = "none";
  }
  imageSearching = false;
}
searchImages();
function findImages() {
  keyword = inputBox.value;
  if (keyword) {
    pages = 1;
    resultImages.innerHTML = null;
    searchImages();
  }
}
inputBox.addEventListener("keyup", (e) => {
  if (e.key == "Enter") {
    findImages();
  }
});
searchIcon.addEventListener("click", findImages);

window.addEventListener("scroll", () => {
  if (!imageSearching) {
    if (
      window.innerHeight + window.scrollY >=
      document.documentElement.offsetHeight - 100
    ) {
      imageSearching = true;
      setTimeout(() => {
        pages++;
        searchImages();
      }, 3000);
    }
  }
});
